import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform
} from "react-native";


export const MainButton = (props) => {

  let ButtonComponent = TouchableOpacity

  if (Platform.OS === "android" && Platform.Version >= 21) {
      ButtonComponent = TouchableNativeFeedback
  }

  return (
    <ButtonComponent activeOpacity={0.6} onPress={props.onPress}>
      <View style={styles.button}>
        <Text View style={styles.buttonText}>
          {props.children}
        </Text>
      </View>
    </ButtonComponent>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#f7287b",
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 25,
  },

  buttonText: {
    color: "white",
    fontSize: 18,
  },
});
