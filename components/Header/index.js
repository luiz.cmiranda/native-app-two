import React from "react"
import {View, Text, StyleSheet, Platform} from 'react-native'


export const Header = (props) => {
    return (
        <View style={styles.header}>
            <Text styles={styles.headerTitle}>
                {props.title}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
  header: {
     width: "100%",
     height: 90,
     paddingTop: 36,
     backgroundColor: Platform.OS === "android" ? "#f7287b" : "white",
     alignItems: "center",
     justifyContent: "center",
     borderBottomColor: Platform.OS === "ios" ? "black" : "transparent",
     borderBottomWidth: Platform.OS === "ios" ?  1 : 0
  },

  headerTitle: {
      color:"black",
      fontSize: 40,
    
  }
})