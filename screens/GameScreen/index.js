import React, { useState, useRef, useEffect } from "react";
import { View, Text, StyleSheet, Dimensions, Alert } from "react-native";
import { Card } from "../../components/Header/Card";
import { MainButton } from "../../components/Header/MainButton";
import { NumberContainer } from "../../components/Header/NumberContainer";

const generateRamdom = (min, max, exclude) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  const randonNumber = Math.floor(Math.random() * (max - min)) + min;
  if (randonNumber === exclude) {
    return generateRamdom(min, max, exclude);
  } else {
    return randonNumber;
  }
};

export const GameScreen = (props) => {
  const [currentGuess, setCurrentGuess] = useState(
    generateRamdom(1, 100, props.userChoice)
  );
  const [rounds, setRounds] = useState(0);
  const currentLow = useRef(1);
  const currentHigh = useRef(100);

  const { userChoice, onGameOver } = props;

  useEffect(() => {
    if (currentGuess === props.userChoice) {
      props.onGameOver(rounds);
    }
  }, [currentGuess, userChoice, onGameOver]);

  const nextGuessHandle = (direction) => {
    if (
      (direction === "menor" && currentGuess < props.userChoice) ||
      (direction === "maior" && currentGuess > props.userChoice)
    ) {
      Alert.alert("Não minta", "Você sabe que isso está errado ...", [
        { text: "Desculpe", style: "cancel" },
      ]);
      return;
    }
    if (direction === "menor") {
      currentHigh.current = currentGuess;
    } else {
      currentLow.current = currentGuess;
    }
    const nextNumber = generateRamdom(
      currentLow.current,
      currentHigh.current,
      currentGuess
    );
    setCurrentGuess(nextNumber);
    setRounds((curRounds) => curRounds + 1);
  };

  return (
    <View style={styles.screen}>
      <Text>Adivinhação do oponente</Text>
      <NumberContainer>{currentGuess}</NumberContainer>
      <Card style={styles.buttonContainer}>
        <MainButton  onPress={nextGuessHandle.bind(this, "menor")} > MENOR </MainButton>
        <MainButton onPress={nextGuessHandle.bind(this, "maior")} > MAIOR </MainButton>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },

  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: Dimensions.get("window").height > 600 ? 20 : 5,
    width: 350,
    maxWidth: "130%",
  },
});
