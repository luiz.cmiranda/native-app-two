import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
} from "react-native";
import { MainButton } from "../../components/Header/MainButton";

export const GameOverScreen = (props) => {
  return (
    <ScrollView>
      <View style={styles.screen}>
        <Text style={styles.text}>Fim de jogo!</Text>
        <View style={styles.imageContainer}>
          <Image
            source={require("../../assets/success.png")}
            style={styles.image}
            resizeMode="cover"
          />
        </View>
        <Text style={styles.text}>Número de Rodadas: {props.roundsNumber}</Text>
        <Text style={styles.text}>O número é: {props.userNumber}</Text>
        <MainButton onPress={props.onRestart}> NOVO JOGO </MainButton>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },

  text: {
    fontSize: 25,
  },

  imageContainer: {
    width: Dimensions.get("window").width * 0.7,
    height: Dimensions.get("window").width * 0.7,
    borderRadius: (Dimensions.get("window").width * 0.7) / 2,
    borderColor: "black",
    borderWidth: 3,
    overflow: "hidden",
    marginVertical: Dimensions.get("window").height / 30,
  },

  image: {
    width: "100%",
    height: "100%",
  },
});
